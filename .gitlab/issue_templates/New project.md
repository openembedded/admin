<!--
     This form lets you request hosting of a new or existing project on
     the OpenEmbedded.org Gitlab group.

     We provide hosting on gitlab.org/openembedded for projects, including Git
     repositories, issue tracking, merge requests and code review.

     Please take the below as a guide and fill in all relevant information it
     requests, but if you would like anything else, if you have any questions
     about any of the services, or just want to note something, please include
     it as freeform text.

     As you fill the sections, please check the boxes at the top; please also
     pay close attention to the contact and CoC sections and check those as
     appropriate.

     Thanks!
-->

## Checklist

- [ ] Project name filled by requestor
- [ ] Project details filled by requestor
- [ ] Project contacts filled by by requestor
- [ ] CoC section filled by requestor
- [ ] GitLab project information filled by requestor

## Project name
<!--
     The project name will be used for the creation on the gitlab instance. The
     project will be created under the Community subgroup, as such the link to
     your project will be:
     https://gitlab.com/openembedded/community/<project name>
-->

**Project name** :

## Project details

<!--
     Please describe in detail what your project is, what it does, and how it
     aligns with OpenEmbedded.org's aims and mission.
-->


## Project contacts
<!--
     This is required for all projects; we will use it this if we need a
     decision representing the project as a whole. For instance, if someone is
     requesting access to your project, if copyright/license issues arise, or if
     we need to discuss something which affects all our member projects (e.g.
     the move to GitLab, or the introduction of the Code of Conduct), we will
     get in touch with those listed here and assume you are able to speak on
     behalf of the project.

     We will also make the primary admin (and additional contacts, if you
     request) the owner of the GitLab group. With this, you will be able to
     completely control access to your repos, and to create new repos, without
     OpenEmbedded.org admin intervention.

     Please list multiple contacts! If you need to amend the contacts, just
     reopen this issue and we'll be happy to help.
-->

**Primary project admin** (GitLab username):

**Additional project contacts** (GitLab usernames):


## Code of Conduct
<!--
     This is required for all projects.

     OpenEmbedded.org maintains a unified Code of Conduct, which can be found at
     https://www.openembedded.org/wiki/Code_of_Conduct

     Our CoC extends across all our platforms - GitLab itself, mailing lists,
     hosted sites, and so on. In short, the CoC requires everyone participating
     in discussion on OpenEmbedded.org to engage in good faith, to treat those
     around them like human beings, and to refrain from bullying behaviour,
     harrassment or discrimination.

     We prefer this to be the standard at project level: that contributors can
     walk into any project and expect that they will be treated with respect.
     Therefore, we recommend projects place a link to the CoC in places like
     README or CONTRIBUTING files (as well as a small mention alongside, e.g.,
     links to mailing lists or GitLab). This should contain a list of
     contacts for your project, who contributors who can raise any issues with.

     Whilst OpenEmbedded.org provides a last-level point of escalation for CoC
     issues, such as when people are uncomfortable raising issues with the project
     team directly, we prefer this to be a part of every project's day-to-day
     communications and ethos, rather than enforced on you from above.
-->

- [ ] My project agrees to the Code of Conduct
- [ ] Where reasonable, we have placed links to the CoC within our project

**CoC contacts** (if different from overall project contacts):


/label ~"New projects"
